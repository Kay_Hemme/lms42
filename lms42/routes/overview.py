from ..app import db, app
from ..models import curriculum
import flask


@app.route('/overview', methods=['GET'])
def showOverview():
    return flask.render_template('overview.html',
        periods=curriculum.get('periods'),
        endterms=get_endterms(),
        modules=curriculum.get('modules_by_id'),
        hbo_i=curriculum.get('hbo_i'),
        outcomes_by_id=curriculum.get('outcomes_by_id'),
    )

def get_endterms():
    endterms = curriculum.get()['endterms']
    nodes = curriculum.get()['nodes_by_id']
    for endterm in endterms:
        for outcome in endterm['outcomes']:
            goals = outcome.get('goals')
            if goals:
                for goal in goals:
                    node_ids = goal.get('node_ids')
                    if node_ids:
                        goal['nodes'] = [nodes[k] for k in node_ids]
    return endterms
