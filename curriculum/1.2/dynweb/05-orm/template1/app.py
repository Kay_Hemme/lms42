from flask_sqlalchemy import SQLAlchemy
import flask
import postgresqlite
import sqlalchemy as sa


app = flask.Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = postgresqlite.get_uri()
db = SQLAlchemy(app)


class Chair(db.Model):
    """A very simple example model,"""
    id = sa.Column(sa.Integer, primary_key=True)
    leg_count = sa.Column(sa.Integer, nullable=False, default=4)
    description = sa.Column(sa.String)


with app.app_context():
    db.create_all()
