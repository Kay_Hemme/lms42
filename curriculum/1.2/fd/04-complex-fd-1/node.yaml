name: A more complex design
description: Create interview questions, a domain model and a UX design for a more complex case study.
goals:
    interview: 1
    requirements: 1
    wireframing: 1
    datamodeling: 1
days: 2
pair: true
assignment:

    - Assignment: |
        In this assignment we take a first step in creating a functional design. The first thing you need to do is understand what the customer wants. 

        Below you will find a video for an application request. Watch the video in which the customer explains what he whishes you to build. Take notes when watching the video!

         - [Application Request: Dating website for animals](https://video.saxion.nl/media/paco-pets/1_yvswd1zd)


    - Goals and tasks:
      -
        text: |
          The first thing to do is to formulate the goal and tasks of the app idea. In `case-pet-dating.md` we will start with the main goal of the app. Try to capture the essence of the app in one line.

          Once you have formulated the main goal of the app you should try to think of the tasks that contribute to this goal. Prioritize the tasks (most important comes first). 

        0: Little to no questions have been created
        4: Relevant and critical questions have been created
        map:
          interview: 1


    - Example mapping:
      -
        text: |
          The template in `case-pet-dating.md` show a number of stories that splits up what you need to specify. A (user) story is simply a label for a conversation you need to have about a particular aspect of the application. 

          For each story you need to create the *rules* and *examples* that specify the behavior of the system relevant for that story.
          
          * Examples should illustrate the rules.
          * Examples should check different instances of applying the rule. You should not include examples that do not check anything new.
          * Rules should summarize the examples.
          * Rules should be exact enough for you to implement them in a computer program
          

        0: Rules and Examples are not relevant for the case described
        4: Consistent rules and relevant examples that specify the desired behavior
        map:
          requirements: 1

    - Formulate questions:
      -
        text: |
          After you have created the rules and examples reflect on whether everything has sufficiently been specified. In `case-pet-dating.md` there is a section *Questions* for which you need to write down: 
          - Questions about whether the rules match the provided case description
          - Questions about what is missing in the provided case description
          - If there are any contradicting rules
        must: true
         

    - Domain model:
      -
        link: https://www.youtube.com/watch?v=jtWj-rreoxs
        title: Map out a Domain Model
        info: Introduction into how map entities from use cases to a domain model.
      -
        text: |
          Create a domain model (logical ERD including the attribute type) for the provided case description:

          - Create the domain model using PlantUML (see the file `case-pet-dating.md` in the template folder).
          - Your domain model should be fully consistent with the case description and your wireframes (next objective).

        0: Domain model does not give a clear impression of the application to be build
        4: Domain contains all entities with proper relationship (including correct cardinality).
        map:
          datamodeling: 1


    - User flows:
      -
        link: https://careerfoundry.com/en/blog/ux-design/how-to-create-a-user-flow/
        title: "How To Create A User Flow: A Step-By-Step Guide"
        info: In this blog the elements of a user flow is explained and the 
      -
        text: |
          Create for the top 3 tasks the user flows. We will use the plantuml flow chart for this. In `case-pet-dating.md` you can write down your user flows.

        0: No user flows
        4: Clear user flow
        map:
          requirements: 2


    - Wireframes:
      -
        text: |
          Create **low fidelity** wireframes for your application, demonstrating how the end product might work (but not what it will look like). Your design should show which visual elements are found in your application screens and how the user can interact with these elements.

          Your tasks:
          - Create a wireframe for every screen in your **phone** application (in `case-pet-dating.wt` or in Quant-UX). Do *not* use standard filler text (like *Lorem ipsum*), but try to come up with some realistic example data. That allows you to verify that all data fields actually make sense, and helps viewers understand how the application will be used in practice.
          - For every screen you should explain (*in a note next to the screen*) the following:
            1. Which elements are interactive (though this should be obvious in your wireframe).
            2. For each interactive element the effect it has in the application. For example a "Save" button in your application will save information in an applications database. You should describe these actions. Although this "Save" example is straightforward some, actions a not (for example a push notification that is sent to a different user when a message is created in the application). Buttons/links that refer to another WireText screen generally don't need any further description.
          - Your wireframes should be fully consistent with the case description and your domain model.

        0: Wireframes do not give a clear impression of the application to be build
        4: Clear wireframes including intuitive navigation between pages.
        map:
          wireframing: 1
        
        