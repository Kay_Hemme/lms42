name: Being an employee
description: What does it mean to be an employee?
goals:
    employee: 1
days: 1
allow_longer: true
avg_attempts: 1
public_for_students: true

resources:
- |
    For this assignment we'll specifically be looking at the laws, rules, regulations and customs in The Netherlands. As quality information in the English language about this subject is scarce, we will for once cite a couple of Dutch language sources. Also, you will have to do some Googling/Ducking/Binging of your own.
-
    link: https://business.gov.nl/regulation/contract-employment/
    title: Business.gov.nl - Employment contract
    info: Explains temporary and permanent employment contracts, and what should at least be included in such a contract.
-
    link: https://www.iamexpat.nl/expat-info/taxation/dutch-tax-system
    title: "Dutch tax system & Taxes in the Netherlands"
    info: The most important types of taxes in The Netherlands. It's worth reading and trying to comprehend all of this document, as unfortunately you'll probably be dealing with most of the taxes mentioned here at some point in your life.
-
    link: https://parakar.eu/nl/payslip-in-the-netherlands-how-does-it-work/
    title: "Payslip in the Netherlands: How does it work?"
    info: When you work for a company, you'll periodically receive a 'loonstrookje', which is notoriously hard to comprehend. This page explains!
-
    link: https://www.berekenhet.nl/werk-en-inkomen/bruto-netto-salaris.html
    title: Bruto-netto salaris berekenen
    info: Tool that helps you convert from *gross* (bruto) to *net* (netto) salaries.
-
    link: https://www.berekenhet.nl/ondernemen/loonkosten-werkgever.html
    title: Loonkosten voor de werkgever
    info: On top of the gross salary, employers pay even more taxes for each employee. This tool helps you calculate how much.
-
    link: https://business.gov.nl/regulation/cao/#:~:text=If%20you%20run%20a%20company,and%20other%20conditions%20of%20employment
    title: CAO (Collective Labour Agreement)
    info: What is a CAO and when does it apply.
-
    link: https://www.fnv.nl/cao-sector
    title: FNV - CAO & Sector
    info: The full list of CAOs (collective labour agreements), by company and by sector.

-
    link: https://bnlegal.nl/en/services/employment-and-labour-law/termination-of-employment-in-the-netherlands/
    title: Termination of employment in The Netherlands

-
    link: https://www.leasevergelijker.nl/nieuwsbericht/wat-kost-een-leaseauto/
    title: LeaseVergelijker - Wat kost een leaseauto?

-
    link: https://www.ondernemenmetpersoneel.nl/motiveren/functioneringsgesprek/handleiding-voor-de-gesprekscyclus
    title: Ondernemen met personeel - Handleiding voor de gesprekscyclus


assignment:
    Exam work: |
        <div class="notification is-warning">
            Please note that this assignment counts as part of the module exam. You *are* allowed to attempt this assignment as many times as is required to get to a passing grade though. (Which will hopefully be just once.)
        </div>

            
    Assignment:
    -
        scale: 10
        6: Answers in the right direction to all except one or two questions.
        7: Student doesn't speak Dutch and provided a couple of reasonable answers.
        9: As good as example solution. Provides good answers to all of the questions.
        map:
            employee: 4
        
        text: |
            For a software developer with a relevant professional diploma (HBO) and five years of experience, working at a media agency in Enschede, please answer the following questions, citing sources where you can:

            - What is her/his gross monthly salary? You may give a range.
            - Based on the gross monthly salary, how much can he/she expect to receive every month?
            - How much vacation money is he/she entitled to?
            - Assuming the company is paying all of its employees a 13th month (bonus), show how you calculate his/her gross yearly salary.
            - Does any CAO apply on this employee? If so, which one?
            - Is the employer required to pay for a pension plan? Why (not)?
            - Assuming the employer *is* providing a pension plan, what would be a ball-park figure that would be put into the pension plan for this employee every month? Would he/she typically pay part of that? If so, what may be a reasonable split?
            - If this employee was not living up to the company's expectations, even after providing lots of time an opportunities to improve, what options does the company have? What are the options for firing somebody, and when can they be used? Any options besides that?
            - When this employee was first hired, would he likely have gotten a permanent or a temporary contract? Why? How many temporary contracts would be allowed, and for what period of time?
            - Suppose the employee receives a brand new Volkswagen Golf (non-electric, no options) as a lease car. Give a ballpark figure of what this would cost the employer per year (excluding fuel costs), and give an approximate calculation for the additional income tax the employee would have to pay based on the *bijtelling*. 

            Students whom are not fluent in Dutch will be graded leniently and/or can ask for extra help from a teacher.
            