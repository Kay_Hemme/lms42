name: Graduation kick-off
description: Once you've obtained 60 ECTS, you should start looking into graduation.  
days: 1
allow_longer: true
type: overhead
grading: false
feedback: false
upload: false

intro:
    Important:
    -
        <div class="notification is-important">Please do your graduation kick-off once you've obtained 60 ECTS!</div>
        <div class="notification"><em>Hint:</em> <a href="https://en.wikipedia.org/wiki/Timeboxing">Timebox</a> this assignment to a single working day! Just use your best guesses.</div>

assignment:

-
    must: true
    text: In order to start planning for your internships, you need to know what the graduation portfolio entails. Please take some time to study the graduation portfolio requirements outlined in *2.4 Portfolio defense*.

-
    must: true
    text: Ask a teacher to become your graduation supervisor, or to help you find one. You can discuss your internship plans with him/her. 

-
    must: true
    text: Schedule a bi-weekly meeting time with your graduation supervisor. This is a 15 minute meeting, during which you'll discuss graduation-related issues, like portfolio opportunities, planning, internships, etc. In advance of every meeting, make sure your portfolio `README.md` (which you'll start working on in the next objective) is up-to-date and pushed to GitLab.

-
    must: true
    text: |
        Create a GitLab repository for your repository as instructed in *2.4 Portfolio defense*. Create a plan in `README.md`, adding as many details as you can.
    
        - We want to see at least an initial schedule for when you're planning to do your remaining modules, your internships, your projects, and a graduation date. Try to make a realistic guess. We understand that there's a lot of uncertainty, so you're allowed (and required) to adjust your plan at any time if necessary.
            - Keep in mind that you'll need to complete your OSS Project before you start your first internship.
            - Plan to have all your regular modules finished *at the latest* before you start your second internship and your second free project.
        - Some initial ideas on what (types of) companies you'd like to intern at, and what (sort of) free projects you'd like to do. It's okay to be vague at this point.
        - Some initial thoughts about in what internship or project each of the portfolio parts can be achieved.

        Don't worry about getting everything in this document right the first time. Best guesses are fine! You'll get to refine everything right up until your graduation. From this point onwards, please try to keep your `README.md` file up-to-date in GitLab.

- Submit this assignment after you've pushed your graduation repository to GitLab. Ask your graduation supervisor to grade the assignment.
