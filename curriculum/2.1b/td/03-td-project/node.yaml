name: Project
description: Design the architecture for a software system, choosing technologies with regard for scalability, reliability and security.
days: 6
allow_longer: true
ects: 5
type: assessment
miller: sh
goal_checks: false
allow_ai: true
depend: cloud
goals:
    design-patterns:
        title: Recognize and apply common design patterns.
        apply: 0
    class-diagram-design:
        title: Design class diagrams for non-trivial systems.
        create: 0
    architecture:
        title: Create high-level overviews of software architectures.
        analyze: 1
    plantuml:
        title: Create graphical software models using PlantUML.
        apply: 1
    decisions:
        title: Make methodical decisions.
        evaluate: 4
    decomposition:
        create: 1
    scalability:
        title: Design scalable systems.
        create: 2
    reliability:
        title: Design reliable systems.
        create: 1
    secure-design:
        title: Design secure systems.
        create: 1
        
about: |
    - This is an exam *project*. As the learning process is part of the project, you're allow to get some help from the teachers. However, for this project you are *not* allowed to consult other students nor to share your work.

    - The recommended duration for this exam is about 5 or 6 working days, but you *must* submit your work after 8 full working days. Weekend days, internship days and vacation days are not counted, of course. You *can* take your laptop home in the mean time.

    - As an entry condition to this project, you're required to have passed the two earlier lessons with an 8 or higher (using however many attempts you need). Their subject matters will not be covered by this project.

    - The subject matter of this project is high-level but very technical. You'll be asked to make decision that you, as a junior developer, will (hopefully) not have to make for production systems any time soon. However, well-functioning teams *do* try to involve their juniors in these types of discussions, and may ask you to make such decision for a prototype/experimental project.
    
    - Rest assured: you will be graded on the level of a junior. It's okay not to know/understand everything and to get some things wrong. But we do expect to see the results of some focussed research, and to see well-reasoned choices.

    - Your grade for this module will be based on your oral defense of your project work. You'll be interviewed by two examiners (also known as teachers). The interview will usually take about 15 minutes.
