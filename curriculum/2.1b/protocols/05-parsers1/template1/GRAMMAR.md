<Program> ::= <Statements>

<Statements> ::= ( <Statement> )*

<Statement> ::= <Declaration> ';' | <Expression> ';' | <IfStatement> | <WhileStatement>

<Block> ::= <Statement> | '{' <Statements> '}'

<Declaration> ::= 'var' IDENTIFIER ( '=' <Expression> )?

<Expression> ::= <Atom> ( OPERATOR <Expression> )?

<Atom> ::= "(" <Expression> ")" | INTEGER | STRING | IDENTIFIER ( "(" ( <Arguments> )? ")" )?

[Arguments] ::= <Expression> ( "," [Arguments] )?

<IfStatement> ::= "if" "(" <Expression> ")" <Block> ( "else" <Block> )?

<WhileStatement> ::= "while" "(" <Expression> ")" <Block>
