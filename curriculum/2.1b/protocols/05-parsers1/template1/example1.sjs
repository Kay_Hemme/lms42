/*
 * Variable declarations
 */

// Let's declare a variable.
// This statement should already be parsed correctly by the template code.
var x;

// And for our next trick, we'll declare a variable an initialize it with an expression.
var y = 3;

// And now a string variable
var z = "z's initial value";