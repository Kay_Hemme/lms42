/*
 * Single statement code blocks without curly brackets.
 */

// Instead of a block containing any number of statements surrounded
// by {curly brackets}, we can also accept a single statement.
var a;
if (0==0)
    a = 1;
else 
    a = 2;

// This should make the 'else if' combination possible without any
// further parsing work.
if (a!=2) {
    a = 3;
}
else {
    a = 4;
}
