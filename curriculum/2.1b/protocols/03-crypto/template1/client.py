import nacl.public
import asyncio


# How to read the server
SERVER = "127.0.0.1"
PORT = 27261


async def main():
    # Ask the user for the secret
    secret = input("Type your secret: ")

    # Open a connection to the server
    reader, writer = await asyncio.open_connection(SERVER, PORT)

    # Write the secret
    writer.write(bytearray(secret, 'utf-8'))

    # Indicate that this is all the server will be receiving
    writer.write_eof()

    # Read, decrypt and print the answer
    print(str(await reader.read(), 'utf-8'))


asyncio.run(main())
