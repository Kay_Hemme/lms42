- |
    Today we are going to work on modules. A module is a file consisting of Python code. A module can define functions, variables and include runnable code. And the most fun part is: you don't always need to write the modules you want to use yourself, as many good ones are available online for free! 
    
    This assignment is done in the context of a video streaming service's marketing agency. They have collected a lot of reviews the last couple of months and are curious how well the content is received they currently are presenting. Up to you as a programmer to help them with that.
    
-   Load the reviews file: 
    -
        link: https://www.geeksforgeeks.org/command-line-arguments-in-python/
        title: Command Line Arguments in Python
    -
        text: |
            The provided `reviews-subset.txt` and `reviews-full.txt` files contain movie reviews, one per each line.  Your program should be able to load a file like this into a list of strings (making sure you don't include the newline characters). Which file is loaded should be specified as the first and only command-line argument to your application. If the user gives zero or more than one argument, the application should exit with an error message.

            ```
            $ python review.py
            Usage: specify a single file name.

            $ python review.py a b
            Usage: specify a single file name.

            $ python review.py no-such-file
            File does not exist.

            $ python review.py reviews-subset.txt
            Loaded 50 reviews.
            ```

        ^merge: feature
    
-   Poetry:
    -   Packages?: |
            Good programmers are lazy programmers. There's no reason to implement something that has already been implemented (better) by someone else. There third-party implementations are usually in the form of libraries, or packages. There are many [Open Source](https://opensource.com/resources/what-open-source) libraries freely available, that'll help you build all sorts of programs.

            However, using these packages in Python can be a bit tricky. The standard tool for installing packages is called _pip_. Unfortunately this tool has some drawbacks, of which the main one is that all packages are installed in a global library. This means that if you have two separate projects that use the same package but require different versions, things tend to break.
            
            In order to solve this problem Python created something called 'virtual environment' (_venv_ for short). For each project you can create a separate _venv_ in order to keep the packages (and their version) separate between projects. While _pip_ en _venv_ solve the problem of isolating used packages in project it can be a pain to set up properly. A more user-friendly (and more feature rich) alternative we will use is called Poetry.
    - 
        link: https://modelpredict.com/python-dependency-management-tools#all-solutions-compared
        title: Overview of python dependency management tools
        info: The world of Python dependency management is a mess, but once you understand the tools and why they exist, it’s going to be easier to choose the one you want and deal with the others in environments where you can’t choose your favorite ones.

    - Installing Poetry: |
        As Poetry is an official Manjaro package, you can install it using *Add/remove software*. It's called `python-poetry`.

        After that, you'll need to issue the following command in your terminal, in order to allow VisualStudio Code to play nice with Poetry. You'll only need to do this once:

        ```sh
        poetry config virtualenvs.in-project true
        ```

    - Getting started with Poetry: |
            In order to manage a project's dependencies using Poetry, you first need to initialize it. In a terminal, from within your project's main directory type:
            ```
            $ poetry init -n
            $ poetry install
            ```

            When you want to run an application in the poetry virtual environment (where the packages are installed) you can use the following command:

            ```
            $ poetry run python review.py
            ```

            For now, this does the same as just running `python review.py`, as we're not using any Poetry-installed packages yet. We'll get to that later.

            Alternatively you can start the poetry shell first - this should only be done once - and afterwards use the regular command to start the application:
            ```
            $ poetry shell
            $ python review.py
            $ python review.py # and run it again!
            ```

            It is also possible to start a debug session now, make sure to select the correct interpreter (`./.venv/bin/python`). This will also solve any `module not found` linting errors.
    - 
        link: https://www.youtube.com/watch?v=V7UhzA4g2yg&t=140s
        title: Using Poetry to manage Python projects
        info: Poetry provides an all-in-one tool for setting up projects, including virtual environments, dependency management, and many other moving parts. Learn the basics in our five-minute introduction.
   
    - |
        <div class="notification is-important">So remember: whenever you read instructions somewhere on the internet to use <em>pip</em>, use <em>poetry</em> instead!</div>

    -
        title: Use poetry
        must: true
        text: |
            Initialize a Poetry project in your assignment directory. Next, use it to install the `textblob` package.

-   Sentiment analysis: 
    -
        link: https://towardsdatascience.com/my-absolute-go-to-for-sentiment-analysis-textblob-3ac3a11d524
        title: Sentiment Analysis using TextBlob

    -
        ^merge: feature
        text: |
            Use `textblob` to print the sentiment polarity for each of the reviews. 
            Use `reviews-subset.txt` to test in the following assignments before running the full set.
            Textblob also requires some trained models and datasets from `NLTK` which can be downloaded via the following command:
            ```
            poetry run python -m textblob.download_corpora lite
            ```
            Note that this is not needed for python modules in general, this is only specific for the textblob package.
-   Create your own module:
    -
        link: https://www.programiz.com/python-programming/modules
        title: Python Modules
        info: In this article, you will learn to create and import custom modules in Python. Also, you will find different techniques to import and use custom and built-in modules in Python.
    -
        link: https://pythonprogramming.net/module-import-syntax-python-3-tutorial/
        title: "Module import Syntax Python Tutorial"
        info: When you import a module, you are basically loading that module into memory. Think of a module like a script. When you go to import it, you use the file name. This can help keep code clean and easy to read.
    -
        link: https://pythonprogramming.net/making-modules/
        title: "Making your own Modules Python Tutorial"
        info: Making your own Modules Python Tutorial
    -
        ^merge: feature
        text: |
            Create a `analysis.py` module within your project directory. In it, create a function that, given a list of (any number of) reviews, returns the three reviews with the highest sentiment polarity.

            Adapt your `review.py` to use this function, and now only printing the three most positive reviews. Also move the relevant sentiment analysis parts to the `analysis.py` module.            
            
-   Keep progress:
    -
        link: https://github.com/tqdm/tqdm#iterable-based
        title: TQDM
        info: Official documentation for the tqdm Python module.
    -
        link: https://www.geeksforgeeks.org/python-ascii-art-using-pyfiglet-module/
        title: Pyfiglets examples
        info: Examples on how to use the `pyfiglet` module to create ASCII art.
    -
        link: https://xkcd.com/303/
        title: xkcd 303
        info: The most legitimate excuse for slacking off as a programmer.  
    -
        ^merge: feature
        text: |
           Install and use the `tqdm` package to show a progress bar while doing sentiment analysis on the list of reviews. This is very welcome when analyzing the full set.
           
           In addition add the `pyfiglet` package to print `Coffee time` or a similar message when a lot of reviews have to be analyzed, as a heads-up to slack off.

-   Leetspeak:
    -
        link: https://www.freecodecamp.org/news/if-name-main-python-example/
        title: Python if __name__ == __main__ Explained with Code Examples
    -
        ^merge: feature
        text: |
            To address programmers more, the marketing department wants to put the top-three reviews on the website *in leetspeak*.

            You already have code for getting the top-three, and you have written code for converting text to *leetspeak* in the third (`funcstr`) lesson. Copy your implementation from `funcstr` to your current project directory. Adapt it such that it can be used both as a stand-alone program (working exactly like it used to) as well as as a module. Use the leetspeak converted function from your application to output the final result.

-   Pie chart:
    -
        link: https://www.geeksforgeeks.org/pie-plot-using-plotly-in-python/
        title: Plotly pie chart examples
        info: Examples on how to create a pie chart with plotly 
    -
        ^merge: feature
        text: |
            Finally, the marketing department also wants a fancy pie chart to present next to the leet-reviews.

            Extend the `analysis.py` module to give back the total amount of positive reviews in the dataset.
            Then install and use the `plotly` package to show a pie chart in the `reviews.py` file. 
            This chart must show the percentage of positive reviews and how it relates to the rest of the data.
