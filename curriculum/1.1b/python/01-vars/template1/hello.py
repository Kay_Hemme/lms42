# Traditionally, this is the first program to run in any new programming
# language you'll learn:
print("Hello world!")

# After you have installed the Python Extension for Visual Studio Code,
# you should be able to run it by pressing F5.

# Use this file to experiment!
