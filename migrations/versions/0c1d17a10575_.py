"""empty message

Revision ID: 0c1d17a10575
Revises: 722ef2d2da2f, deb712a55a08
Create Date: 2023-11-14 17:47:19.808643

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0c1d17a10575'
down_revision = ('722ef2d2da2f', 'deb712a55a08')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
