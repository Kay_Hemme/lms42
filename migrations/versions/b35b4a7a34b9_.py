"""Add default_schedule to user table

Revision ID: b35b4a7a34b9
Revises: efd78d153ce8
Create Date: 2022-04-21 11:30:50.931871

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b35b4a7a34b9'
down_revision = 'efd78d153ce8'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('user', sa.Column('default_schedule', sa.ARRAY(sa.String()), nullable=False, server_default='{"present","present","present","present","present"}'))
    
    # Convert the old absent_days to 'home' in default_schedule
    for day in range(5):
        op.execute(f"""UPDATE "user" SET default_schedule[{day+1}]='home_structural' WHERE {day} = ANY(absent_days);""")
    
    op.drop_column('user', 'absent_days')


def downgrade():
    op.add_column('user', sa.Column('absent_days', sa.ARRAY(sa.Integer()), nullable=False, server_default='{}'))

    # Add the 'structural' days as absent_days
    for day in range(5):
        op.execute(f"""UPDATE "user" SET absent_days = array_append(absent_days,{day}) WHERE default_schedule[{day+1}] IN ('home_structural', 'absent_structural');""")

    op.drop_column('user', 'default_schedule')
